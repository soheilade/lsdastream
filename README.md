# README #

This project consists of 3 sections: 

first in the clientSrever package we aim to simulate a client server scenario for streaming data. To do so, first the StreamServer.java class should be executed. StreamServer will start reading airpollotion,traffic and parking data from the provided files in the form of CSV in streams/ folder and pushing them. A client subscription is simulated by running the ClientEndPoint.java. In the main function of this class we handshake with the server and send the name of the stream to subscribe to.  Afterwards the client will start receiving streamed triples from server and prints them into the console or pushes them to the CSPARQL RDF stream processor. If received streams are pushed into CSPARQL, they can be queried to be processed in more details. This is done in CSPARQLClient.java.
To print the result of CSPARQL query to output, FileFormatter is designed. it can be added as an observer to CSPARL Result Proxy. 

Second, in the twitter package, MyTwitterStream extends RdfStream to get the tweet from tweeter and pushs triples to CSPARQL for processing arbitrary queries. MyTwitterStream at the moment only pushes user mention informations from tweets into CSPARQL and it can be modified to pushes more key values from tweet into CSPARQL. TwitterClient initializes the CSPARQL engine and an RDFStream of type MyTwitterStream and process arbitrary queries. it prints the result of CSPARQL query in a file. However, the result of CSPARQL queries can also be pushed into ClingoFormatter.java similar to FileFormatter to perform reasoning.

Third, in the ASPReasoning package, ClingoHandler.java passes logic programs into clingo via a java program. note that  ClingoFormatter.java fetches the reason function with dynamic content of windows as facts and a static logic program. However both facts and rule can be dynamically modified.


### How do I get set up? ###
First replace the ****..* in MyTwitterStream.java with your twitter keys for your application.
This is a maven project, however given that some underlying projects don't have entries in maven repositories or because i modified the original version of project(e.g. CityBench) i put the libraries that are needed to run the project in the lib folder. In the resources folder we have the compatible clingo version and logic program needed to run the ClingoHandler.java and TwitterClient.java with ClingoFormatter. 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

soheila (sally.de at gmail.com)