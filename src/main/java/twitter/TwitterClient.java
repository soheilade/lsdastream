package twitter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.DeploymentException;
import javax.websocket.Session;

import org.glassfish.tyrus.client.ClientManager;

import ASPReasoning.ClingoHandler;
import clientServer.FileFormatter;
import eu.larkc.csparql.cep.api.RdfStream;
import eu.larkc.csparql.engine.ConsoleFormatter;
import eu.larkc.csparql.engine.CsparqlEngine;
import eu.larkc.csparql.engine.CsparqlEngineImpl;
import eu.larkc.csparql.engine.CsparqlQueryResultProxy;
import twitter.MyTwitterStream;

public class TwitterClient {
	public static void main(String[] args) {

		String query2 = "REGISTER QUERY MyQuery AS " + "PREFIX ex: <http://myexample.org/> " 
				+ "SELECT ?s (COUNT(*) AS ?mentions) "
				+ "FROM STREAM <http://myexample.org/stream> [RANGE 10s STEP 5s] "
				+ "WHERE { ?s <http://myexample.org/isMentioned> ?o  }"
				+ "GROUP BY ?s "
				+ "HAVING (COUNT(*) > 1)"
			;
		

		// initializations

		String streamURI = "http://myexample.org/stream";
		//RdfStream tg1,tg2;
		MyTwitterStream tq1;
		try {
			/*tg1 = new ClientEndPoint(streamURI, new URI("ws://localhost:8085/websockets/subscribe"));
			
			tg2 = new ClientEndPoint(streamURI, new URI("ws://localhost:8085/websockets/subscribe"));
*/
			tq1=new MyTwitterStream(streamURI);
			
			// Initialize C-SPARQL Engine

			CsparqlEngine engine = new CsparqlEngineImpl();

			engine.initialize(false);

			// Register an RDF Stream

			engine.registerStream(tq1);
			//engine.registerStream(tg2);

			Thread.sleep(10000);
			/*final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().build();
			final ClientManager client = ClientManager.createClient();
			final Session session = client.connectToServer(tg1, cec, new URI(endpointURI));
			final Session session2 = client.connectToServer(tg2, cec, new URI(endpointURI));
			final CountDownLatch messageLatch = new CountDownLatch(1000);
			messageLatch.await(1, TimeUnit.SECONDS);
			session.getBasicRemote().sendText("CSPARQLAarhusPollutionStream");
			session2.getBasicRemote().sendText("CSPARQLAarhusParkingStream");
			*/
			
			CsparqlQueryResultProxy c1 = null;
			CsparqlQueryResultProxy c2 = null;

			try {
				c1 = engine.registerQuery(query2);
				tq1.sample();
				System.out.println("Query: " + query2);
				System.out.println("Query Start Time : " + System.currentTimeMillis());
			} catch (final ParseException ex) {
				System.out.println("errore di parsing: " + ex.getMessage());
			}

			// Attach a Result Formatter to the query result proxy

			if (c1 != null) {
				c1.addObserver(new FileFormatter(new BufferedWriter(new FileWriter(new File("/output.txt")))));//new ClingoFormatter(new ClingoHandler()));//new ConsoleFormatter());//new FileFormatter(new BufferedWriter(new FileWriter(new File("/home/soheila/output.txt")))));
			}

			// leave the system running for a while
			// normally the C-SPARQL Engine should be left running
			// the following code shows how to stop the C-SPARQL Engine gracefully
			try {
				Thread.sleep(200000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			System.exit(0);
		/*} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (DeploymentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();*/
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
