package twitter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import eu.larkc.csparql.cep.api.RdfQuadruple;
import eu.larkc.csparql.cep.api.RdfStream;
import twitter4j.FilterQuery;
import twitter4j.JSONObject;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterObjectFactory;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.UserMentionEntity;
import twitter4j.conf.ConfigurationBuilder;

public class MyTwitterStream extends RdfStream{
	TwitterStream tStream ;
	private ConfigurationBuilder cb;

	
	public MyTwitterStream(String iri) {
		super(iri);
		cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
		.setOAuthConsumerKey("***********************")
		.setOAuthConsumerSecret("****************************")
		.setOAuthAccessToken("**********************")
		.setOAuthAccessTokenSecret("********************************");
		cb.setJSONStoreEnabled(true);
		listen();
	}
	public void listen(){
		try{
			tStream = new TwitterStreamFactory(cb.build()).getInstance();
			// sample() method internally creates a thread which manipulates TwitterStream and calls these adequate listener methods continuously.
			StatusListener listener = new StatusListener() {

				public void onException(Exception ex) {

					ex.printStackTrace();
				}

				public void onTrackLimitationNotice(int arg0) {
					// TODO Auto-generated method stub

				}

				public void onStatus(Status status) {
					try {String rawJSON = TwitterObjectFactory.getRawJSON(status);
					JSONObject jsonObj = new JSONObject(rawJSON);
					//System.out.println(jsonObj.getInt("id"));		
					
					for(UserMentionEntity ume : status.getUserMentionEntities()){		
									/*JSONObject[] x=(JSONObject[]) ((JSONObject)jsonObj.get("entities")).get("user_mentions");
									for(JSONObject j : x){
									*/final RdfQuadruple q1 = new RdfQuadruple(
											/*"http://twitter.com/" +*/ Long.toString(ume.getId()),
											"http://myexample.org/isMentioned", 
											"http://myexample.org/tweet" +jsonObj.getInt("id") , 
											System.currentTimeMillis());
									MyTwitterStream.this.put(q1);
									/*}*/
									/*fw.write(rawJSON+"\n");
									fw.flush();*/
								
							}	} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}					            				
				}

				public void onStallWarning(StallWarning stallWarning) {
					System.out.println(stallWarning);				
				}

				public void onScrubGeo(long arg0, long arg1) {
					// TODO Auto-generated method stub				
				}

				public void onDeletionNotice(StatusDeletionNotice arg0) {
					// TODO Auto-generated method stub
				}
			};	
			tStream.addListener(listener);
			
			//fw.flush();

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public void sample(){
		tStream.sample();
	}
}
