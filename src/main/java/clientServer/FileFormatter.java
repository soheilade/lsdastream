package clientServer;

import java.io.BufferedWriter;

import eu.larkc.csparql.common.RDFTable;
import eu.larkc.csparql.common.RDFTuple;
import eu.larkc.csparql.common.streams.format.GenericObservable;
import eu.larkc.csparql.core.ResultFormatter;

public class FileFormatter  extends ResultFormatter  {

	private BufferedWriter bw;
	public FileFormatter (BufferedWriter x){
		bw=x;
	}
	public void update(GenericObservable<RDFTable> arg0, RDFTable arg1) {
			RDFTable q = (RDFTable) arg1;
		try{
			for (final RDFTuple t : q) {
			bw.write(t.toString());
		}
			bw.write(">>>>>>>>>>\n");
			bw.flush();
			
		}catch(Exception e){e.printStackTrace();}
		
	}

}
