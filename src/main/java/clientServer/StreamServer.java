package clientServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.glassfish.tyrus.server.Server;
import org.insight_centre.aceis.eventmodel.EventDeclaration;
import org.insight_centre.aceis.eventmodel.TrafficReportService;
import org.insight_centre.aceis.io.ServerStreamListener;
import org.insight_centre.aceis.io.rdf.RDFFileManager;
import org.insight_centre.aceis.io.streams.ResultStream;
import org.insight_centre.aceis.io.streams.csparql.CSPARQLAarhusParkingStream;
import org.insight_centre.aceis.io.streams.csparql.CSPARQLAarhusPollutionStream;
import org.insight_centre.aceis.io.streams.csparql.CSPARQLAarhusTrafficStream;
import org.insight_centre.aceis.io.streams.csparql.CSPARQLSensorStream;

import com.google.gson.Gson;

@ServerEndpoint(value = "/subscribe")//, decoders = { }, encoders = { })
public class StreamServer {
	private Logger logger = Logger.getLogger(this.getClass().getName());
	public static String hostName = "localhost";
	public static int port = 8085;
	public static String contextPath = "/websockets";
	private Map<String, Object> properties;
	private static HashMap<String,CSPARQLSensorStream> streams=new HashMap<String,CSPARQLSensorStream>();
	public StreamServer(){
		createThreads();		
	}
	@OnOpen
	public void onOpen(Session session, EndpointConfig config)
			throws IOException {
		logger.info("onOpen: " + session.getId());
		properties = config.getUserProperties();
		logger.info("properties: " + properties.toString());
	}
	
	private void createThreads() {
		try {
			List<String> payloads1 = new ArrayList<String>();
			payloads1.add(RDFFileManager.ctPrefix + "API|"+RDFFileManager.defaultPrefix + "FoI-1|"
					+ RDFFileManager.defaultPrefix + "Property-1" );
			EventDeclaration ed1 = new EventDeclaration("testEd", "testsrc", "air_pollution", null, payloads1, 5.0);
			ed1.setServiceId(RDFFileManager.defaultPrefix+"AirPollution1");
			ServerStreamListener ssl1=new ServerStreamListener();
			//ssls.add(ssl);
			CSPARQLAarhusPollutionStream aps1 = new CSPARQLAarhusPollutionStream("testuri",
					"streams/AarhusPollutionData158324.stream", ed1,ssl1);
			Thread th1 = new Thread(aps1);
			streams.put(aps1.getClass().getSimpleName(),aps1);
			th1.start();
			
			List<String> payloads2 = new ArrayList<String>();
			payloads2.add(RDFFileManager.ctPrefix + "parking|"+RDFFileManager.defaultPrefix + "FoI-1|"
					+ RDFFileManager.defaultPrefix + "Property-2" );
			EventDeclaration ed2 = new EventDeclaration("testEd", "testsrc", "air_pollution", null, payloads2, 5.0);
			ed2.setServiceId(RDFFileManager.defaultPrefix+"Parking1");
			ServerStreamListener ssl2=new ServerStreamListener();
			CSPARQLAarhusParkingStream aps2 = new CSPARQLAarhusParkingStream("testuri",
					"streams/AarhusParkingData.stream", ed2,ssl2);
			Thread th2 = new Thread(aps2);
			//System.out.println(aps2.getClass().getSimpleName());
			streams.put(aps2.getClass().getSimpleName(),aps2);
			th2.start();
			//serverThreads.add(th);
			List<String> payloads3 = new ArrayList<String>();
			payloads3.add(RDFFileManager.ctPrefix + "AvgSpeed|"+RDFFileManager.defaultPrefix + "FoI-1|"
					+ RDFFileManager.defaultPrefix + "Property-3" );
			TrafficReportService ed3 = new TrafficReportService("testEd", "testsrc", "traffic", null, payloads3, 5.0);
			ed3.setServiceId(RDFFileManager.defaultPrefix+"traffic1");
			ServerStreamListener ssl3=new ServerStreamListener();
			CSPARQLAarhusTrafficStream aps3 = new CSPARQLAarhusTrafficStream("testuri",
					"streams/AarhusTrafficData201345.stream", ed3,ssl3);
			Thread th3 = new Thread(aps3);
			//System.out.println(aps2.getClass().getSimpleName());
			streams.put(aps3.getClass().getSimpleName(),aps3);
			th3.start();			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@OnClose
	public void onClose(Session session, CloseReason reason) throws IOException {
		logger.info("onClose: " + session.getId());
	}

	@OnError
	public void onError(Session session, Throwable t) {
		logger.info("onError: " + session.getId());
		t.printStackTrace();
	}

	@OnMessage
	public void onMessage(String message, Session session) {
		logger.info("JsonWrapper onMessage: " + session.getId());
		parseMessageAndSendRelevantDataToClient(message,session);		
		
	}
	private void parseMessageAndSendRelevantDataToClient(String message,Session session) {
		ResultStream rs= new ResultStream(streams.get(message),session);
			
	}
	public void runServer() {

		Server server = new Server(hostName, port, contextPath,
				StreamServer.class);
		try {
			server.start();
			
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					System.in));
			logger.info("Please press a key to stop the server.");
			reader.readLine();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			server.stop();
			//stops the threads
		}
	}

	

	public static void main(String[] args) {
		System.out.println("hello");
		new StreamServer().runServer();
	}
}
