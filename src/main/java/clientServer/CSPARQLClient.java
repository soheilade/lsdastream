package clientServer;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.DeploymentException;
import javax.websocket.Session;

import org.glassfish.tyrus.client.ClientManager;

import eu.larkc.csparql.cep.api.RdfStream;
import eu.larkc.csparql.engine.ConsoleFormatter;
import eu.larkc.csparql.engine.CsparqlEngine;
import eu.larkc.csparql.engine.CsparqlEngineImpl;
import eu.larkc.csparql.engine.CsparqlQueryResultProxy;
import eu.larkc.csparql.ui.TextualFormatter;
import twitter.MyTwitterStream;

public class CSPARQLClient {
	public static void main(String[] args) {

		String query2 = "REGISTER QUERY MyQuery AS " + "PREFIX ex: <http://myexample.org/> " 
				+ "SELECT ?s ?p ?o "
				+ "FROM STREAM <http://myexample.org/stream> [RANGE 5s STEP 1s] "
				+ "WHERE { ?s ?p ?o  }"
				;
		String endpointURI = "ws://localhost:8085/websockets/subscribe";

		// initializations

		String streamURI = "http://myexample.org/stream";
		RdfStream tg1,tg2;
		try {
			tg1 = new ClientEndPoint(streamURI, new URI("ws://localhost:8085/websockets/subscribe"));
			
			tg2 = new ClientEndPoint(streamURI, new URI("ws://localhost:8085/websockets/subscribe"));

			
			// Initialize C-SPARQL Engine

			CsparqlEngine engine = new CsparqlEngineImpl();

			engine.initialize(false);

			// Register an RDF Stream

			engine.registerStream(tg1);
			engine.registerStream(tg2);

			Thread.sleep(10000);
			final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().build();
			final ClientManager client = ClientManager.createClient();
			final Session session = client.connectToServer(tg1, cec, new URI(endpointURI));
			final Session session2 = client.connectToServer(tg2, cec, new URI(endpointURI));
			final CountDownLatch messageLatch = new CountDownLatch(1000);
			messageLatch.await(1, TimeUnit.SECONDS);
			session.getBasicRemote().sendText("CSPARQLAarhusPollutionStream");
			session2.getBasicRemote().sendText("CSPARQLAarhusParkingStream");
			
			
			CsparqlQueryResultProxy c1 = null;
			CsparqlQueryResultProxy c2 = null;

			try {
				c1 = engine.registerQuery(query2);
				System.out.println("Query: " + query2);
				System.out.println("Query Start Time : " + System.currentTimeMillis());
			} catch (final ParseException ex) {
				System.out.println("errore di parsing: " + ex.getMessage());
			}

			// Attach a Result Formatter to the query result proxy

			if (c1 != null) {
				c1.addObserver(new ConsoleFormatter());
				
			}

			// leave the system running for a while
			// normally the C-SPARQL Engine should be left running
			// the following code shows how to stop the C-SPARQL Engine gracefully
			try {
				Thread.sleep(200000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			System.exit(0);
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (DeploymentException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
