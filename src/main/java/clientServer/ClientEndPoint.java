/**
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER
 */
package clientServer;

import java.io.IOException;
import java.net.URI;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import javax.websocket.ClientEndpoint;
import javax.websocket.ClientEndpointConfig;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import org.glassfish.tyrus.client.ClientManager;

import eu.larkc.csparql.cep.api.RdfQuadruple;
import eu.larkc.csparql.cep.api.RdfStream;


@ClientEndpoint()
public class ClientEndPoint extends  RdfStream {
	private Logger logger = Logger.getLogger(this.getClass().getName());
	private Map<String, Object> properties;
	Session userSession = null;
	
	public ClientEndPoint(String uri) {
		super(uri);		
	}
	
	public ClientEndPoint(String uri,URI endpointURI) {
		super(uri);
		try {
			WebSocketContainer container = ContainerProvider
					.getWebSocketContainer();
			container.connectToServer(this, endpointURI);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	@OnOpen
	public void onOpen(Session session, EndpointConfig config)
			throws IOException {
		logger.info("onOpen: " + session.getId());
		properties = config.getUserProperties();
		logger.info("properties: " + properties.toString());
	}

	@OnClose
	public void onClose(Session session, CloseReason reason) throws IOException {
		logger.info("onClose: " + session.getId());
	}

	@OnError
	public void onError(Session session, Throwable t) {
		logger.info("onError: " + session.getId());
		t.printStackTrace();
	}

	@OnMessage
	public void onMessage(String message, Session session) {
		System.out.println("client received the text");
		System.out.println(message);
		String[] timeAndData=message.split(": ");
		if(timeAndData[0].equalsIgnoreCase("update")){
		//System.out.println(timeAndData[2]);
		String[] data = timeAndData[2].split("£");
		//System.out.println(data.length);
		/*for(String s : data)
			System.out.println(s);*/
		
		for(int i=0;i<data.length;i++){
			String[] elements=data[i].split(", ");
			//System.out.println("putting " +data[i]+" in server"+ elements[0]);
			//System.out.println(elements[1]);
			//System.out.println(">>>>>>>>>>pushing "+elements[0]+" "+elements[1]+" "+ elements[2]+" "+Long.parseLong(timeAndData[1])+" to csparql");
			this.put(new RdfQuadruple(elements[0], elements[1], elements[2], Long.parseLong(timeAndData[1])));
		}
		}else{
			
		}
	}	
	
	public void sendMessage(String message) {
		
		this.userSession.getAsyncRemote().sendText(message);
	}

	public void close() {
		try {
			this.userSession.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void main(String args[]){
		try{
		String streamURI = "http://myexample.org/stream";
		String endpointURI = "ws://localhost:8085/websockets/subscribe";

		ClientEndPoint tg2 = new ClientEndPoint(streamURI, new URI(endpointURI));

		final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().build();
		final ClientManager client = ClientManager.createClient();
		final Session session2 = client.connectToServer(tg2, cec, new URI(endpointURI));
		final CountDownLatch messageLatch = new CountDownLatch(1000);
		messageLatch.await(1, TimeUnit.SECONDS);
		session2.getBasicRemote().sendText("CSPARQLAarhusPollutionStream");
		while(true){}
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
