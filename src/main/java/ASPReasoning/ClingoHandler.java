package ASPReasoning;

/*
 * Download Clingo at: http://potassco.sourceforge.net
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;


public class ClingoHandler{

	/**
	 *
	 */
	public ClingoHandler() {
		super();
	}

	/*
	 * This function calls Clingo from Java
     * Param: program: is a set of facts/rules that is parsed from Java
     * Param: filePaths: is a list of file about ASP rules
     * Note: Clingo can reason on rules written in files or from string.
	 */
	
	public void reason(String program, List<String> filesPaths) throws IOException, InterruptedException {

        //create a command to call Clingo
		final StringBuilder commandStringBuilder = new StringBuilder();
        
        //getClingoPath() returns the path of Clingo
		commandStringBuilder
				.append("/resources/clingo-4.5.3-linux-x86_64/clingo")
				.append(' ');
		commandStringBuilder/*.append(options)*/.append(' ');
        
        // add files to command
		for (final String string : filesPaths) {
			commandStringBuilder.append(' ').append(string);
		}
        
        
		commandStringBuilder.append(" -");

		System.out.println("Calling Clingo: " + commandStringBuilder.toString());

        // Give command to Runtime object
		final Runtime instance = Runtime.getRuntime();
			final Process process = instance.exec(
				commandStringBuilder.toString());

        // add input to Clingo
        final BufferedWriter bufferedWriter = new BufferedWriter(
				new OutputStreamWriter(process.getOutputStream()));
		bufferedWriter.write(program);
		bufferedWriter.close();

		process.waitFor();
		System.out.println("Called Clingo");

		// read output of Clingo
		final BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(process.getInputStream()));
        
        // read error of Clingo
		final BufferedReader hexerr = new BufferedReader(new InputStreamReader(
				process.getErrorStream()));

        
		String clingoOutput = "";
		String currentLine;
		String currentErrLine;
        
        //print output
		while ((currentLine = bufferedReader.readLine()) != null) {
			clingoOutput += currentLine + "\n";
		}
		System.out.println(clingoOutput);
        
        // in case has error in reasoning
		while ((currentErrLine = hexerr.readLine()) != null) {
			System.out.println(currentErrLine);
		}	
	}
	public static void main(String[] args) throws IOException, InterruptedException{
		ClingoHandler ch=new ClingoHandler();
		//ch.reason("", Arrays.asList("/home/soheila/TA/reasoningASSignment/exampleASP.txt 0"));
		ch.reason("", Arrays.asList("/home/soheila/TA/reasoningASSignment/ASP-assignment5a.lp 0"));
	}
	
}
